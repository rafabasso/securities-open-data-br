# Open data about Brazil's capital market

Trying to create a perfectly reproducible public dataset about Brazil's
capital market

## Rationale

To elaborate, review and reproduce research papers, readily available
[data](https://en.wikipedia.org/wiki/Open_science_data) is fundamental.
Currently, as far as we know, there is no comprehensive,
organized, freely available dataset about the brazilian capital market,
allowing fully reproducible research papers based on stock returns and
traded volumes.

Unfortunately, researchers usually depend on preprocessed information from
nonfree sources, like [Bloomberg](https://www.bloomberg.com/)
and [Economatica](https://economatica.com/). This has allowed limited
reproduceability, assuming that other researchers have access to the same
services.

While those data-vendors may be trustworthy and their services convenient
for market professionals, there are some caveats:

- Information available can change at any time, without notice.
- Data is preprocessed using undocumented, unverifiable algorithms.
- There is no way to ascertain that the extracted data matches the data
produced by the sources.

By scientific standards, the current situation is undesirable. It trades
three of the four mertonian norms - universalism, communism and organized
skepticism - by convenience and trust.

What follows is an attempt at giving a small step in direction of a more
[open science](https://en.wikipedia.org/wiki/Open_science).

## What information do we want?

To be able to fully replicate most of the studies that do not depend on
intraday data or financial statements, we need to have at least...

- [x] Dialy-summarized about stock trading activity
- [ ] Index compositions over time
- [ ] Corporate events
- [ ] Material events

### BM&FBovespa's historical quote data

#### Contents

By far, the most comprehensive data source about the brazilian capital market
is the BM&FBovespa, that daily provides a file with summarized trade
information about every single
[ticker](https://en.wikipedia.org/wiki/Ticker_symbol) traded there. The same
information is consolidated montly and later by year, and these
yearly-consolidated, daily-aggregated trading data is avaible for each year
from 1986.

While it is great to have more than 30 years of raw trading data, the dataset
has some limitations:

- The data uses a non-standard format.
- Some trading information is missings or invalid.
- It may be possible to identify when corporate events happened, but there
isn't enough information to know what was the event and adjust the series
accordingly.

#### Availability

Historical quote data is freely available at the
[BM&FBovespa website](http://www.bmfbovespa.com.br/). Unfortunately,
URLs are not a reliable resource for referencing data sources, because the
links and contents can change at any time.

Also, it is impossible to assure that the data will be indefinitely available
there, and this poses a risk to our ability to reproduce a research that
depends on that data.

To deal with these problem, you'll find some critical information in the table
below:

- A link to download the file from the
[Internet Archive](https://archive.org/), which previously downloaded the file
directly from the BM&FBovespa's website.
- A [cryptographic hash](https://en.wikipedia.org/wiki/Cryptographic_hash_function)
of each mentioned file, to assure that its contents are complete and where not
changed.

| Filename                          | Size (Bytes) | URL                                                                                                                                                                    | SHA-256                                                          |
|:----------------------------------|-------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------|
| `HistoricalQuotations_Layout.pdf` |       46.065 | [Internet Archive](https://web.archive.org/web/20170323041646/http://www.bmfbovespa.com.br/lumis/portal/file/fileDownload.jsp?fileId=8A828D294E9C618F014EB79B6BB3435C) | `8e6f185790d49df6733d8163f4131162414348ca1971445e75b0453b0c8ac95e` |
| `SeriesHistoricas_Layout.pdf`     |       47.345 | [Internet Archive](https://web.archive.org/web/20170323041603/http://www.bmfbovespa.com.br/lumis/portal/file/fileDownload.jsp?fileId=8A828D294E9C618F014EB7924B803F8B) | `5493932eaeb4dfc300854ce6ee7190b26a6eacd54edb4a88f3751393106f0c8e` |
| `COTAHIST_A1986.zip`              |    8.713.632 | [Internet Archive](https://web.archive.org/web/20170323041944/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1986.zip)                                      | `350e6086c8f991484832ca3cd23e900b692769bfd3311017800231fd896c8018` |
| `COTAHIST_A1987.zip`              |    6.349.827 | [Internet Archive](https://web.archive.org/web/20170323043645/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1987.zip)                                      | `16db2bbda8cf770ad177af762c6e651422dee7b1dc7a62ea75fa10e5760f517d` |
| `COTAHIST_A1988.zip`              |    8.122.653 | [Internet Archive](https://web.archive.org/web/20170323043904/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1988.zip)                                      | `b99563d58d2c58ba4c910545fc969041499a45e4e2f6649829a67702193a88ee` |
| `COTAHIST_A1989.zip`              |    7.510.968 | [Internet Archive](https://web.archive.org/web/20170323045854/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1989.zip)                                      | `9f2d468dbbd9ff2ffcf960501cbb0a66d3921ecd7fbb9305a8c6269f302c503d` |
| `COTAHIST_A1990.zip`              |    5.352.193 | [Internet Archive](https://web.archive.org/web/20170323051510/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1990.zip)                                      | `85dd3ad24b0d9742c0d438e91ecec2ff07ef38e381c075469b9d0829c9f95619` |
| `COTAHIST_A1991.zip`              |    5.476.429 | [Internet Archive](https://web.archive.org/web/20170323051753/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1991.zip)                                      | `9c83ede3d78ca1588309f0934c7cbfdbdfd047287c69b44d17ddfa43e4e82262` |
| `COTAHIST_A1992.zip`              |    5.214.684 | [Internet Archive](https://web.archive.org/web/20170323052741/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1992.zip)                                      | `50a35c8c409ee49bfb0fcc1773f687548af85e1c2bf81756f7ef2b89ee035939` |
| `COTAHIST_A1993.zip`              |    5.893.559 | [Internet Archive](https://web.archive.org/web/20170323052954/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1993.zip)                                      | `f63c13905d712e74f69610511d8000b4231644a89bd60d47af1e21209709ecad` |
| `COTAHIST_A1994.zip`              |    5.998.158 | [Internet Archive](https://web.archive.org/web/20170323053319/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1994.zip)                                      | `b3bbd8e8d290c36943398c6f99a04e0721db2ebac3b917904df9efaacf3172eb` |
| `COTAHIST_A1995.zip`              |    5.277.341 | [Internet Archive](https://web.archive.org/web/20170323053454/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1995.zip)                                      | `de553f41a3ce15ec8a082ed1c2d451df58520a447d3f39417f45e7feb5a995fc` |
| `COTAHIST_A1996.zip`              |    5.598.339 | [Internet Archive](https://web.archive.org/web/20170323053638/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1996.zip)                                      | `911a993548931f56b1b3dc18d2994c67f5d011cf7a85986029384298f17d6800` |
| `COTAHIST_A1997.zip`              |    6.094.245 | [Internet Archive](https://web.archive.org/web/20170323053844/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1997.zip)                                      | `f6bd380e753ec8024857b739b7d9b010c7467a5fd967e82d01186a46f5e33fc3` |
| `COTAHIST_A1998.zip`              |    5.384.840 | [Internet Archive](https://web.archive.org/web/20170323054103/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1998.zip)                                      | `91a2d516686a0d0ff133254df2595f44d23d9b7ecf1966a11ef4c2beeacec440` |
| `COTAHIST_A1999.zip`              |    6.675.159 | [Internet Archive](https://web.archive.org/web/20170323054338/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1999.zip)                                      | `fe7428b367a2d40acaefe823fd5080254d07f9ad6c29fc64c9e6876cbd876f09` |
| `COTAHIST_A2000.zip`              |    6.689.526 | [Internet Archive](https://web.archive.org/web/20170323054504/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2000.zip)                                      | `77ba58b5138e384be140759bf836e8cc22189cdfecdf80e4c13672c535f42bd6` |
| `COTAHIST_A2001.zip`              |    4.978.023 | [Internet Archive](https://web.archive.org/web/20170323054601/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2001.zip)                                      | `0386ca1b80d207ef9876fe64c2d48e9af1b827b3ecc0365c0e99db5cacf20eb5` |
| `COTAHIST_A2002.zip`              |    5.877.292 | [Internet Archive](https://web.archive.org/web/20170323054726/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2002.zip)                                      | `ee002b9a533d310351ed07ccb3a8cd3badd84993a4969c5b58088d738de59116` |
| `COTAHIST_A2003.zip`              |    6.901.239 | [Internet Archive](https://web.archive.org/web/20170323054825/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2003.zip)                                      | `bcd337ebc1d92c190c4d8b77f582d2ec0890a8705fd5c10d68b28fa1eb15a2f0` |
| `COTAHIST_A2004.zip`              |    7.902.413 | [Internet Archive](https://web.archive.org/web/20170323054933/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2004.zip)                                      | `f750ba8ee9e25299ed96811979befb29315d91667fedc3885c709440b4989467` |
| `COTAHIST_A2005.zip`              |    8.533.024 | [Internet Archive](https://web.archive.org/web/20170323055037/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2005.zip)                                      | `41eac6d743d9c3e812bb001c4d327582fa7c408dbc99ff4a1734698da8b18f52` |
| `COTAHIST_A2006.zip`              |    9.318.595 | [Internet Archive](https://web.archive.org/web/20170323055207/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2006.zip)                                      | `b5432089f8788ea122b4102cd1f09b1a0a3436c176b8f8c548eef50ec3e02687` |
| `COTAHIST_A2007.zip`              |   10.530.129 | [Internet Archive](https://web.archive.org/web/20170323055423/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2007.zip)                                      | `3c46f425112f838f3eb09023c987d7e5e81500610d3e5e40fedefb52d3dffd66` |
| `COTAHIST_A2008.zip`              |   10.330.640 | [Internet Archive](https://web.archive.org/web/20170323060333/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2008.zip)                                      | `b4d92bcea3c253ab9379ad6ddcb42115c8afb72fd94d39ded2b5912528cf3786` |
| `COTAHIST_A2009.zip`              |    9.620.954 | [Internet Archive](https://web.archive.org/web/20170323060840/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2009.zip)                                      | `bb9421050bfd98a91b6023c1df540e43f421734c6fa0834ab2891c30af581adc` |
| `COTAHIST_A2010.zip`              |   10.385.444 | [Internet Archive](https://web.archive.org/web/20170323061127/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2010.zip)                                      | `65637311ced65ba54a617ab9cc2543974fdd4ed0ce416a5235e01b2f1aae66f5` |
| `COTAHIST_A2011.zip`              |   10.856.136 | [Internet Archive](https://web.archive.org/web/20170323062003/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2011.zip)                                      | `0cf67787cc1b77dde67a4ff1a340123ab261ce252245d24b5d32ec5b8b162359` |
| `COTAHIST_A2012.zip`              |   11.883.689 | [Internet Archive](https://web.archive.org/web/20170323062150/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2012.zip)                                      | `98771b1248a3f19b547609ed4fd6c8da0eb396140afb6de91946db457c855465` |
| `COTAHIST_A2013.zip`              |   13.300.034 | [Internet Archive](https://web.archive.org/web/20170323062253/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2013.zip)                                      | `b6dfb0d73181a60cc65083c2b185a1cbb0c95b87b21f873cc151515c65e47636` |
| `COTAHIST_A2014.zip`              |   13.684.612 | [Internet Archive](https://web.archive.org/web/20170323062329/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2014.zip)                                      | `613d76f8a64c7ffba0406cb54c1fd187266f55ed8b6037cb85993b2071a5aae7` |
| `COTAHIST_A2015.zip`              |   14.129.467 | [Internet Archive](https://web.archive.org/web/20170323062413/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2015.zip)                                      | `593f8740ec1aa87c80c54c623f1759530b6b32cdadd34a64f0fc66a529f34e35` |
| `COTAHIST_A2016.zip`              |   15.715.361 | [Internet Archive](https://web.archive.org/web/20170323062453/http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2016.zip)                                      | `ffc82a9b973cb5901e68dc11bea1bbc9bf0a2c46749e0efe5a7a89229bc0d81f` |

#### Data Structure

The `COTAHIST` file's structure is explained in
`HistoricalQuotations_Layout.pdf`, in English, and
`SeriesHistoricas_Layout.pdf`, in Portuguese.

Here is some additional information not covered in those files:

- The files are mostly ASCII encoded, with some exceptional ISO-8859-1
(latin1) characters.
- While each *register* has exactly 245 bytes, they are always followed by a
[CRLF newline](https://en.wikipedia.org/wiki/Newline). This allows interpreting
files as CRLF separated *registers* or 247-byte blocks.
- Numeric fields are left-padded with zeroes when shorter.
- When a numeric field is missing, or doesn't make sense in the *register*,
it is encoded as a zero.
- Text fields are always right-padded with spaces.
- Some fields are in fact numeric: `CODBDI`, `PRAZOT` and `DISMES`.
- `PRAZOT` can be `000` or blank when missing.
- Date fields that are missing or doesn't make sense in the *register* are
encoded as `9999-12-31`

#### Invalid data

The files from 1986 to 1995 have some *registers* with invalid data in
`PRAZOT`. The folder named `patch` contains
[diff](https://en.wikipedia.org/wiki/Diff_utility) files that substitute the
invalid entries by blanks.

## TO-DO

- Add program to transform sources in `.csv` or `.json`.
- Add Nix expression to build the datasets automatically.
- Add more data sources.
